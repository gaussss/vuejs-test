from django.contrib.auth.models import User
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.hashers import make_password
from rest_framework.mixins import (
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.viewsets import (
    GenericViewSet
)

from .serializers import (
    UserSerializer,
    ProductSerializer
)
from .models import Product
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes

from django.http import JsonResponse
from django.shortcuts import render


def index(request):
    return render(request, "index.html") # render(request, template_name='index.html')


def index_customer(request, sender, receiver):
    return render(request, template_name='index.html')


class CResponse(Response):

    def __init__(self, msg, status):
        self.message = msg
        self.status = status
        Response.__init__(
            self,
            {
                "detail": self.message
            },
            self.status
        )


class BadRequest(CResponse):

    def __init__(self, exception_instance):
        CResponse.__init__(self, repr(exception_instance), status.HTTP_400_BAD_REQUEST)


class UnAutorizedRequest(CResponse):
    
    def __init__(self, exception_instance):
        CResponse.__init__(self, repr(exception_instance), status.HTTP_401_UNAUTHORIZED)


class GoodResponse(CResponse):
    
    def __init__(self, msg):
        CResponse.__init__(self, msg, status.HTTP_200_OK)


class CustModelViewset(
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    GenericViewSet
):
    pass


class ProductviewSet(CustModelViewset):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    #permission_classes = [IsAuthenticated]

    @action(detail=False, methods=['POST'])
    def add_product(self, request):
        data = request.data
        try:
            product = Product.objects.create(**data)
            product.save()
            return GoodResponse(ProductSerializer(product).data)
        except Exception as e:
            return BadRequest(e)
    
    @action(detail=True, methods=['PUT'])
    def update_product(self, request, pk):
        data = request.data
        try:
            product = Product.objects.filter(pk=pk)
            if data["image"] == "" or data["image"] == " ":
                data["image"] = product[0].image
            product.update(**data)
            return GoodResponse(ProductSerializer(product).data)
        except Exception as e:
            return BadRequest(e)


class UserViewSet(CustModelViewset):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=False, methods=['POST'])
    def add_user(self, request):
        data = request.data
        data["password"] = make_password(data["password"])
        try:
            user = User.objects.create(**data)
            user.save()
            return GoodResponse(UserSerializer(user).data)
        except Exception as e:
            return BadRequest(e)
    
    @action(detail=True, methods=['PUT'])
    def update_user(self, request, pk):
        data = request.data
        if data["password"] != "":
            data["password"] = make_password(data["password"])
        else:
            data.pop("password")
        try:
            user = User.objects.filter(pk=pk)
            user.update(**data)
            return GoodResponse(UserSerializer(user).data)
        except Exception as e:
            return BadRequest(e)
