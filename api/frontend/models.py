from django.db import models

class Product(models.Model):
    name = models.TextField(null=True, blank=True)
    category = models.TextField(null=True, blank=True)
    quantity = models.FloatField(null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    image = models.TextField(null=True, blank=True)
    date_at = models.DateTimeField(auto_now=True) 