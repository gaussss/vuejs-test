import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        registerDialog: false,
        loaderDialog: false,
        profilDialog: false,
        addProductDialog: false,
        currentUser: sessionStorage.length == 0 || sessionStorage.getItem("key") == "" ? null : JSON.parse(window.atob(sessionStorage.getItem("key").split(".")[1])).user_id,
        msg: ""
    },
    mutations: {},
    actions: {},
    modules: {}
})