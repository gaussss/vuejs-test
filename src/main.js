import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from '@/plugins/vuetify' // path to vuetify export
//import devtools from '@vue/devtools'

Vue.config.productionTip = false

//devtools.connect("localhost", 8098)

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')