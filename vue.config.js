module.exports = {
    devServer: {
        proxy: 'http://vuetestlevel.herokuapp.com/'
    },

    // outputDir must be added to Django's TEMPLATE_DIRS
    outputDir: './dist/',

    // assetsDir must match Django's STATIC_URL
    assetsDir: 'static',

    transpileDependencies: [
        'vuetify'
    ]
}


/*
module.exports = {
  devServer: {
    proxy: {
      '^/api/': {
        target: 'http://vuetestlevel.herokuapp.com/',
        ws: false,
      }
    }
  },

  // outputDir must be added to Django's TEMPLATE_DIRS
  outputDir: './dist/',

  // assetsDir must match Django's STATIC_URL
  assetsDir: 'static',

  transpileDependencies: [
    'vuetify'
  ]
}
*/